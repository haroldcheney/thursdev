var http = require('http');
var nodeStatic = require('node-static');
var path = require('path');
var fileServer = new nodeStatic.Server(path.join(__dirname, 'public'));
var server = http.createServer(function(request, response) {
    if (request.url == '/') {
        request.url = '/index.html';
    }
    request.addListener('end', function() {
        fileServer.serve(request, response, function(err, result) {
            if (err) { // There was an error serving the file
                response.writeHead(err.status, err.headers);
                response.write("Error: " + err.status);
                response.end();
            }
        });
    });
});

if (process.env.PORT && process.env.IP) {
    server.listen(process.env.PORT, process.env.IP);
}
else if (process.env.VMC_APP_PORT) {
    server.listen(process.env.VMC_APP_PORT);
}
else {
    server.listen(8888);
}
