The demos are currently running here, but I make no guarantees for how long:
http://thursdev.aws.af.cm

To clone the repository:
git clone https://bitbucket.org/haroldcheney/thursdev.git

The repostitory contains two directories, web and upserve.

web project
-----------
This project contains a simple node.js web server to serve the demos.

Before running for the first time, you must install dependencies using:
npm install

The server will listen on the AppFog or Cloud9 host and port if found,
otherwise it will run on port 8888 of the local host. To run:
node server.js

If running on your local host, browse to:
http://localhost:8888

Demos
-----
hello.html
This is a simple Angular.js page that demonstrates data binding.

hellfire.html
This expands on the previous example by adding a Firebase backing
store. To get the full effect of this demo, open in multiple
browser windows to see how the input field is synchronized across
all clients.

Note that my firebase url is hard-coded in this demo, which I
don't guarantee will remain available, so you might want to
register with firebase and change it to your own url.

upper.html
This demonstrates writing requests to firebase to be consumed
by a server-based process, and displaying the results asynchronously.
You can change the firebase url in upper.js to your own.
This demo expects upserve to be running.

supper.html
This is a variation of the previous demo that waits for the response
and displays it synchronously.
You can change the firebase url in supper.js to your own.
This demo expects upserve to be running.

upserve project
---------------
This project contains a simple node.js process that monitors the firebase
and processes requests made by the upper and supper demos.
You can change the firebase url in server.js to your own.

Before running for the first time, you must install dependencies using:
npm install

To run:
node server.js
