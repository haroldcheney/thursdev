angular.module('upper', ['firebase'])
    .controller('UpperCase', ['$scope', 'angularFireCollection',
        function($scope, angularFireCollection) {
            var url = 'https://hc-upper.firebaseIO.com/messages';
            $scope.messages = angularFireCollection(url, $scope, 'messages', []);
            $scope.submitMessage = function() {
                $scope.messages.add({text: $scope.input, status: 'Submitted', submitted: Date.now()});
                $scope.input = "";
            };
        }]);
