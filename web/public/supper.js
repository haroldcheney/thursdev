angular.module('upper', [])
    .controller('UpperCase', ['$scope',
        function($scope) {
            var url = 'https://hc-upper.firebaseIO.com/messages';
            var messagesRef = new Firebase(url);
            $scope.submitMessage = function() {
                var messageRef = messagesRef.push({text: $scope.input, status: 'Submitted', submitted: Date.now()});
                $scope.result = "";
                $scope.processing = true;
                messageRef.on('value', function (snapshot) {
                    var message = snapshot.val();
                    if (message.status == "Completed") {
                        $scope.$apply(function () {
                            $scope.result = message.text;
                            $scope.input = "";
                            $scope.processing = false;
                            messageRef.off();
                        });
                    }
                });
            };
        }]);
