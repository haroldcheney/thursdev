var amount = parseInt(process.argv[2]),
    combinations = 0,
    pennies,
    nickels,
    dimes,
    quarters;
    
console.log("Making change for %d cents:", amount);

for (pennies = 0; pennies <= amount; pennies++) {
    for (nickels = 0; nickels <= amount / 5; nickels++) {
        for (dimes = 0; dimes <= amount / 10; dimes++) {
            for (quarters = 0; quarters <= amount / 25; quarters++) {
                if (pennies + (nickels * 5) + (dimes * 10) + (quarters * 25) === amount) {
                    console.log("%d pennies, %d nickels, %d dimes, %d quarters", pennies, nickels, dimes, quarters);
                    combinations++;
                }
            }
        }
    }
}

console.log("%d combinations found", combinations);
