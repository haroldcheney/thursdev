My solutions to the kata:
"Function to calculate the number of ways n cents can be represented with
quarters, dimes, nickels, and pennies"

coins1.js
---------
Sample usage:

$ node coins1.js 8
Making change for 8 cents:
3 pennies, 1 nickels, 0 dimes, 0 quarters
8 pennies, 0 nickels, 0 dimes, 0 quarters
2 combinations found

This is a simple implementation, but adding new coins requires more work.
For each new coin, a new nested loop must be added, and the condition in the
inner loop must be modified. The value of each coin appears in two places.
This could be simplified by not dividing the amount by the coin's value in the
loop condition, but that would unnecessarily increase the search space.

coins2.js
---------
Sample usage:

$ node coins2.js 8
Making change for 8 cents:
1 nickel, 3 pennies
8 pennies
2 combinations found

In this recursive implementation, the coin definitions are in a separate module,
coindef.js, so it is much easier to add new coins.
For example, if you uncomment the tuppence line:

$ node coins2.js 8
Making change for 8 cents:
4 tuppence
1 nickel, 1 tuppenny, 1 penny
3 tuppence, 2 pennies
1 nickel, 3 pennies
2 tuppence, 4 pennies
1 tuppenny, 6 pennies
8 pennies
7 combinations found

This search space is more efficiently pruned in this implementation, because
it stops searching a coin as soon as adding any more of them exceeds the amount,
rather than always searching a fixed number of each coin, as coins1.js does.

The output of this implementation is friendlier, omitting unused coins,
and using singular and plurals appropriately.

*** UPDATE ***
At the meeting, Ralph asked if it could handle ha'pence. We tried, and it did,
but in order to support the entry of fractional amounts on the command line, we
had to change parseInt to parseFloat.

$ node coins2.js 7.5
Making change for 7.5 cents:
1 nickel, 1 tuppenny, 1 ha'penny
3 tuppence, 1 penny, 1 ha'penny
1 nickel, 2 pennies, 1 ha'penny
2 tuppence, 3 pennies, 1 ha'penny
1 tuppenny, 5 pennies, 1 ha'penny
7 pennies, 1 ha'penny
3 tuppence, 3 ha'pence
1 nickel, 1 penny, 3 ha'pence
2 tuppence, 2 pennies, 3 ha'pence
1 tuppenny, 4 pennies, 3 ha'pence
6 pennies, 3 ha'pence
1 nickel, 5 ha'pence
2 tuppence, 1 penny, 5 ha'pence
1 tuppenny, 3 pennies, 5 ha'pence
5 pennies, 5 ha'pence
2 tuppence, 7 ha'pence
1 tuppenny, 2 pennies, 7 ha'pence
4 pennies, 7 ha'pence
1 tuppenny, 1 penny, 9 ha'pence
3 pennies, 9 ha'pence
1 tuppenny, 11 ha'pence
2 pennies, 11 ha'pence
1 penny, 13 ha'pence
15 ha'pence
24 combinations found

And yes, I know that I'm mixing U.S. and (old) U.K. coinage. I'm sure that's
acceptable somewhere in the world, like maybe Canadia.
