module.exports = [
    {singular: "quarter",   plural: "quarters",  value: 25},    
    {singular: "dime",      plural: "dimes",     value: 10},    
    {singular: "nickel",    plural: "nickels",   value: 5},    
    {singular: "tuppenny",   plural: "tuppence",   value: 2},    
    {singular: "penny",     plural: "pennies",   value: 1},    
    {singular: "ha'penny",     plural: "ha'pence",   value: 0.5}    
]
