var coins = require("./coindef.js"),
    amount = parseFloat(process.argv[2]),
    combinations = 0,
        
    search = function (index, sum) {
        var coin = coins[index];
        
        for (coin.count = 0; sum + (coin.count * coin.value) <= amount; coin.count++) {
            if (index > 0) {
                search(index - 1, sum + (coin.count * coin.value));    
            } else if (sum + (coin.count * coin.value) === amount) {
                output();
                combinations++;
            }
        }        
    },
    
    output = function () {
        var message = "";
        coins.forEach(function(element) {
            if (element.count > 0) {
                if (message.length > 0) {
                    message += ", ";
                }
                message += element.count + " ";
                if (element.count === 1) {
                    message += element.singular;
                } else {
                    message += element.plural;
                }
            }
        });
        console.log(message);
    };

console.log("Making change for %d cents:", amount);
search(coins.length - 1,  0);
console.log("%d combinations found", combinations);
