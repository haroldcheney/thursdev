Brick Kata
----------

The goal is to output a random layout of bricks. A brick is a rectangle that
is twice as long as it is wide.

Given an input n, generate a random layout that meets these criteria:

* The layout is n brick-lengths wide and n brick-lengths tall.
* The layout consists only of whole horizontal and/or vertical bricks,
  no partial bricks are allowed.
* The layout must be symmetrical both right and left, and up and down.
* Vertical bricks are rendered using two pipes each on the right and left sides,
  and one underscore each on the top and bottom sides.
* Horizontal bricks are rendered using one pipe each on the right and left sides,
  and three underscores each on the top and bottom sides, except that the middle
  underscore is omitted if it would be perpendicular to an adjacent pipe.
* The pipe and underscore characters are shared between adjacent bricks.

For example, here are two possible outputs for n = 2:

 _ ___ _
| |___| |
|_|___|_|
| |___| |
|_|___|_|

 ___ ___
|_ _|_ _|
| |___| |
|_|_ _|_|
|___|___|
