var args = process.argv.slice(2),
    LinkedList = require("linkedlist"),
    list = new LinkedList(),
    front,
    back,
    count;

// initialize list
args.forEach(function (arg) {
    list.push(arg);
});

// find middle
front = list._head;
back = list._tail;
count = 1;
while (front !== back) {
    front = front.next;
    count += 1;
    if (front === back) {
        break;
    }
    back = back.prev;
    count += 1;
}
console.log("%s is middle of %d items", front.data, count);
