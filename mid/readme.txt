My solutions to the kata:
"Function to locate the middle element of a doubly linked list without
iterating over a node more than once, and without using a method like size()"

Setup:
$ npm install linkedlist

mid1.js
-------
Sample usage:
$ node mid1.js foo bar baz ban bux
baz is middle of 5 items

Although I don't think this solution is what the author of the kata intended,
I believe that it conforms to a literal interpretation of the requirements,
assuming that you can choose in advance an upper bound for the size of the list,
which is a reasonable requirement in the real world.

It simply walks the list, copying each element to an array and keeping track of
the number of elements in the list, after which it is a simple matter to
calculate and access the midpoint in constant time. This is a classic trade off
of space for time.

Note that achieving a constant time lookup depends on the implementation of the
array. Typical JavaScript engines will use linear storage until some heuristic
causes them to switch to a hash or btree. I believe that the way the array is
populated in sequence here will ensure that V8 uses linear storage.

Also, initializing the array with a size isn't strictly necessary, but my hope
is that it will avoid any need for the array to resize, which might reduce or
elimininate any performance gain from using the array.

mid2.js
-------
Sample usage:
$ node mid2.js foo bar baz ban bux                                       
baz is middle of 5 items

My guess is that this is more like what the author of the kata intended, but
it does not conform to a literal interpretation of the requirements, because
the nodes in the first half of the list are iterated over twice, albeit in
parallel. However, it is difficult to contrive a sitution where this parallel
iteration would be better than a sequential iteration, and I think the code
would be clearer with a sequential iteration.

*** UPDATE ***
At the meeting, both Ralph and Doug proposed iterating simultaneously
from the head and tail until you meet in the middle. I agree that is almost
certainly the solution that the author of the kata intended, so never mind...

mid3.js
-------
Sample usage:
$ node mid3.js foo bar baz ban bux
baz is middle of 5 items

This is an implementation of the strategy discussed at the meeting.
