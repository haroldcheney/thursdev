var args = process.argv.slice(2),
    LinkedList = require("linkedlist"),
    list = new LinkedList(),
    count,
    array;

// initialize list
args.forEach(function (arg) {
    list.push(arg);
});

// find middle
count = 0;
array = new Array(10000);
while (list.next()) {
    array[count] = list.current;
    count += 1;
}
console.log("%s is middle of %d items", array[Math.floor(count/2)], count);
