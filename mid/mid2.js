var args = process.argv.slice(2),
    LinkedList = require("linkedlist"),
    list = new LinkedList(),
    count,
    middle;

// initialize list
args.forEach(function (arg) {
    list.push(arg);
});

// find middle
count = 0;
middle = list._head;
while (list.next()) {
    count += 1;
    if (count % 2 === 0) {
        middle = middle.next;
    }
}
console.log("%s is middle of %d items", middle.data, count);
