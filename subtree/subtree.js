var util = require("util"),
    tree = JSON.parse(process.argv[2]),
    subtree = JSON.parse(process.argv[3]),
    
    equalTree = function (tree1, tree2) {
        var result = false;
        if (tree1 === tree2) {
            result = true;
        } else if (util.isArray(tree1) && util.isArray(tree2) && tree1.length === tree2.length) {
            result = tree1.every(function (element, index) {
                return equalTree(element, tree2[index]);
            });
        }
        return result;
    },
    
    isSubtree = function (tree1, tree2) {
        var result = false;
        if (equalTree(tree1, tree2)) {
            result = true;
        } else if (util.isArray(tree1)) {
            result = tree1.some(function (element) {
                return isSubtree(element, tree2);
            });
        }
        return result;
    };

if (isSubtree(tree, subtree)) {
    console.log("%j is a subtree of %j", subtree, tree);
} else {
    console.log("%j is not a subtree of %j", subtree, tree);
}   

