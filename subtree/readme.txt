My solutions to the kata:
"Function to determine if a tree is a subtree of another tree"

subtree.js
-------
Sample usage:
$ node subtree.js [1,[2,3],[4,5,6]] [2,3]
[2,3] is a subtree of [1,[2,3],[4,5,6]]

The trees in this example are represented by nested arrays. Note that for tree1
to be considered a subtree of tree2, it must be substitutable for a complete
node of tree2, so [2] is not a subtree of [1,2,3].

One nice thing about using Node.js is that unlike client-side JavaScript, you
don't need to code defensively for whatever JavaScript engines the client might
be running, so you can confidently use ECMAScript 5 features that V8 supports,
such as the every() and some() functions of Array.
