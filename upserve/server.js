var Firebase = require('firebase');
var messagesRef = new Firebase('https://hc-upper.firebaseIO.com/messages');
var COMPLETED = "Completed";

messagesRef.on('child_added', function(snapshot) {
    var messageRef = snapshot.ref();
    var message = snapshot.val();
    if (message.status != COMPLETED) {
        console.log("Processing: text=[%s]", message.text);
        messageRef.update({status: 'Processing', started: Date.now()});
        setTimeout(function() {
            var text = message.text ? message.text.toUpperCase() : "";
            messageRef.update({status: COMPLETED, completed: Date.now(), text: text});
        }, Math.floor((Math.random() * 10000) + 1000));
    }
});